FROM openjdk:17
ADD target/docker-0.0.1-SNAPSHOT.jar app.jar
LABEL authors="orestklymko"
EXPOSE 8080
ENTRYPOINT ["java", "-jar","app.jar"]