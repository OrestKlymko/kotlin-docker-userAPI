package com.example.docker.service

import com.example.docker.exception.UserNotFoundException
import com.example.docker.model.User
import com.example.docker.model.UserRequest
import com.example.docker.repository.UserRepo
import org.springframework.stereotype.Service


@Service
class UserService(val userRepo: UserRepo) {


    fun getAllUsers(): List<User> {
        return userRepo.findAll()
    }


    fun getUserById(id: Long): User {
        return userRepo.findById(id).orElseThrow { UserNotFoundException("Not found $id") }
    }

    fun createUser(userRequest: UserRequest): User {
        return userRepo.save(User().userRequestToCreatingUser(userRequest))
    }

    fun deleteUserById(id: Long) {
        val user = userRepo.findById(id).orElseThrow { UserNotFoundException("Not found $id") }
        userRepo.delete(user);
    }

    fun updateUserById(id: Long, userRequest: UserRequest): User {
        val user = userRepo.findById(id).orElseThrow { UserNotFoundException("Not found $id") }
        user.name = userRequest.name;
        user.email = userRequest.email;
        user.password = userRequest.password;
        return userRepo.save(user);
    }
}