package com.example.docker.model

import jakarta.persistence.*
import org.springframework.validation.annotation.Validated

@Entity
@Table(name = "users")
class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    var id: Long = 0L
        private set

    @Column(name = "name")
    var name: String = ""

    @Column(name = "email")
    var email: String = ""

    @Column(name = "password")
    var password: String = ""


    fun userRequestToCreatingUser(userRequest: UserRequest): User {
        val newUser = User()
        newUser.email = userRequest.email;
        newUser.name = userRequest.name;
        newUser.password = userRequest.password;
        return newUser;
    }
}
