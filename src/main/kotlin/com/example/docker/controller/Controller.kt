package com.example.docker.controller

import com.example.docker.exception.UserNotFoundException
import com.example.docker.model.User
import com.example.docker.model.UserRequest
import com.example.docker.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/v1/users")
class Controller(var userService: UserService) {


    @GetMapping
    fun getAllUsers(): ResponseEntity<List<User>> {
        return ResponseEntity.ok().body(userService.getAllUsers());
    }

    @GetMapping("/{id}")
    fun getUserById(@PathVariable id: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok().body(userService.getUserById(id))
        } catch (ex: UserNotFoundException) {
            ResponseEntity.status(404).body(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping("/create")
    fun createUser(@RequestBody userRequest: UserRequest): ResponseEntity<User> {
        return ResponseEntity.ok().body(userService.createUser(userRequest))
    }

    @PutMapping("/update/{id}")
    fun updateUser(@RequestBody userRequest: UserRequest, @PathVariable id: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok().body(userService.updateUserById(id, userRequest))
        } catch (ex: UserNotFoundException) {
            ResponseEntity.status(404).body(HttpStatus.NOT_FOUND)
        }
    }

    @DeleteMapping("/delete/{id}")
    fun deleteUser(@PathVariable id: Long): ResponseEntity<Any> {
        return try {
            userService.deleteUserById(id)
            ResponseEntity.ok().body("User deleted")
        } catch (ex: UserNotFoundException) {
            ResponseEntity.status(404).body(HttpStatus.NOT_FOUND)
        }
    }
}

