package com.example.docker.exception

class UserNotFoundException(message: String?) : Exception(message) {
}