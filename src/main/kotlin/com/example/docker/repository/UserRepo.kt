package com.example.docker.repository

import com.example.docker.model.User
import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository

@Repository
interface UserRepo : JpaRepository<User, Long> {
}